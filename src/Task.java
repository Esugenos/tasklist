import java.io.*;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

public class Task {
    File repertoire = new File("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save");
    ArrayList<String> tList = null;
    String taskListNameFileChange;

    public Task() {

        System.out.println("Entrez votre nom:");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        Scanner sc2 = new Scanner(System.in);
        String answerTot = null;
        do {

            System.out.println(name + ", quelle action souhaitez-vous effectuer?");
            int index = sc.nextInt();
            do {
                System.out.println("1 - créer une nouvelle liste de tâche\n" +
                        "2 - Supprimer une liste de tâche existante\n" +
                        "3 - Modifier une liste de tâche existante\n\n Indiquez l'index");

                switch (index) {
                    case 1:
                        System.out.println("Quel nom voulez-vous donner à votre liste de tâches?");
                        String taskListNameFileCreate = sc.nextLine();
                        AddTaskList(taskListNameFileCreate);
                        break;
                    case 2:
                        String[] fileList;
                        fileList = repertoire.list();
                        if (fileList.length == 0) {
                            System.out.println("Il n'existe aucune liste de tâches à supprimer");
                        } else {
                            System.out.println("Quelle liste de tâches souhaitez-vous supprimer?");
                            ListRepertoire(repertoire);
                            String taskListNameFileRemove = sc.nextLine();
                            removeTaskList(taskListNameFileRemove);
                        }
                        break;
                    case 3:
                        System.out.println("Quelle liste souhaitez-vous modifier?");
                        File repertoire = new File("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save");
                        ListRepertoire(repertoire);

                        String taskListNameFileChange = sc.nextLine();
                        this.taskListNameFileChange=taskListNameFileChange;
                        File x = new File("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save\\" + taskListNameFileChange + ".txt");
                        if (!x.exists()) {
                            System.out.println("Cette liste de tâche n'existe pas!");
                        }
                        ChangeTaskList(taskListNameFileChange);
                        String answerRestart = sc.nextLine();
                        do {
                            System.out.println("Quel action souhaitez-vous réaliser?\n 1 - Ajouter une tâche\n 2 - Supprimer une tâche");
                            int index2 = sc.nextInt();
                            if (index2 == 1) {
                                String answer = sc.nextLine();
                                do {
                                    System.out.println("Quelle tâche souhaitez-vous ajouter?");
                                    String taskName = sc.nextLine();
                                    tList.add(taskName);
                                    System.out.println("Voulez-vous en ajouter une autre? [O/N]");
                                } while (answer.toLowerCase().equals("o"));
                            }else if (index2 == 2) {
                                    String answer2 = sc.nextLine();
                                    do {
                                        System.out.println("Quelle tâche souhaitez-vous supprimer?");
                                        String taskName = sc.nextLine();
                                        tList.remove(taskName);
                                        System.out.println("Voulez-vous en supprimer une autre? [O/N]");
                                    } while (answer2.toLowerCase().equals("o"));
                                }else {
                                    System.out.println("Je ne reconnais pas cette action");
                            }
                            System.out.println("voulez-vous réaliser une autre action sur votre liste " + taskListNameFileChange + " ? [O/N]");
                        } while (answerRestart.toLowerCase().equals("o"));
                        break;
                    default:
                        System.out.println("Je ne comprends pas votre demande");
                }
            } while (index != 1 || index != 2 || index != 3);
            System.out.println("Très bien, voulez-vous sauvegarder vos actions?  [O/N]");
            String answerSave = sc.nextLine();
            if (answerSave.toLowerCase().equals("o")) {
                saveList(taskListNameFileChange);
            } else {
                System.out.println("Toutes les actions ont été annulées.");
            }
            System.out.println("Voulez-vous quitter l'application? [O/N]");
            answerTot=sc2.nextLine();
        } while (answerTot.toLowerCase().equals("o"));
        System.out.println("Fin de l'application");
    }

    public void AddTaskList(String NameTaskList) {
        try {
            new Formatter("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save\\" + NameTaskList + ".txt");
            ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File(("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save\\" + NameTaskList + ".txt")))));
            ArrayList<String> newTaskList = new ArrayList<>();
            oos.writeObject(newTaskList);
            oos.close();
        } catch (FileNotFoundException e) {
            System.out.println("Erreur de création du fichier");
        } catch (IOException e) {
            ;
            System.out.println("Erreur de création de la liste de tâches");
        }
    }

    public void removeTaskList(String NameTaskList) {
        File file = new File("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save\\" + NameTaskList + ".txt");
        file.delete();
    }

    public ArrayList ChangeTaskList(String NameTaskList) {

        try {
            ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save\\" + NameTaskList + ".txt")))));
            tList = (ArrayList<String>) ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Erreur d'ouverture du flux vers le fichier demandé");
        }
        return tList;
    }

    public void ListRepertoire(File repertoire) {
        String[] fileList;
        int i;
        fileList = repertoire.list();
        for (i = 0; i < fileList.length; i++) {
            System.out.println(fileList[i]);
        }

    }
    public void saveList(String NameTaskList){
        try {
            new Formatter("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save\\" + NameTaskList + ".txt");
            ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File(("C:\\Users\\NEDELEC\\Desktop\\workspace\\Generator\\save\\" + NameTaskList + ".txt")))));
            oos.writeObject(tList);
            oos.close();
        } catch (FileNotFoundException e) {
            System.out.println("Erreur de sauvegarde de la liste des tâches");
        } catch (IOException e) {
            ;
            System.out.println("Erreur de sauvegarde de la liste des tâches");
    }
}}
